﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RimWorld;
using Verse;

namespace YouWillDie
{
    public class ChoiceLetter_RansomOrBombDemandYWD : ChoiceLetter
    {
        public Map map;

        public int fee = 1000;

        public override IEnumerable<DiaOption> Choices
        {
            get
            {
                if (base.ArchivedOnly)
                {
                    yield return base.Option_Close;
                }
                else
                {
                    DiaOption accept = new DiaOption("RansomDemand_Accept".Translate());
                    accept.action = delegate
                    {
                        TradeUtility.LaunchSilver(this.map, this.fee);
                        MapComponent_OrbitalBombardmentYWD bombardmentComp = map.GetComponent<MapComponent_OrbitalBombardmentYWD>();
                        bombardmentComp.openRansomDemand = false;
                        Find.LetterStack.RemoveLetter(this);
                    };
                    accept.resolveTree = true;
                    if (this.map == null || !TradeUtility.ColonyHasEnoughSilver(this.map, this.fee))
                    {
                        accept.Disable("NeedSilverLaunchable".Translate(this.fee.ToString()));
                    }
                    yield return accept;

                    DiaOption reject = new DiaOption("RejectLetter".Translate());
                    reject.action = delegate
                    {
                        MapComponent_OrbitalBombardmentYWD bombardmentComp = map.GetComponent<MapComponent_OrbitalBombardmentYWD>();
                        bombardmentComp.ransomDeadlineTick = Find.TickManager.TicksGame;
                        Find.LetterStack.RemoveLetter(this);
                    };
                    reject.resolveTree = true;
                    yield return reject;

                    yield return base.Option_Postpone;
                }
            }
        }

        public override bool CanShowInLetterStack
        {
            get
            {
                return base.CanShowInLetterStack && (this.map == null || Find.Maps.Contains(this.map));
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_References.Look<Map>(ref this.map, "map", false);
            Scribe_Values.Look<int>(ref this.fee, "fee", 1000, false);
        }
    }
}
