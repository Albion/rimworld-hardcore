﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RimWorld;
using Verse;

namespace YouWillDie
{
    public static class FactionAdditionYWD
    {
        public static bool EnsureFactionExists(FactionDef factionDef)
        {
            if (Find.FactionManager.FirstFactionOfDef(factionDef) != null)
            {
                return true;
            }

            try
            {
                Faction faction = FactionGenerator.NewGeneratedFaction(factionDef);
                Find.FactionManager.Add(faction);
                return true;
            }
            catch
            {
                Log.Message("Something went wrong with creating a new faction for the You Will Die Mod.");
            }
            return false;
        }
    }
}
