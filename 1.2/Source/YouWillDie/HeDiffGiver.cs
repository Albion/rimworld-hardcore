﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RimWorld;
using Verse;

namespace YouWillDie
{
    public class HediffGiver_GlitterCombatBoost : HediffGiver
    {

        public override void OnIntervalPassed(Pawn pawn, Hediff cause)
        {
            if (pawn.Faction != null)
            {
                if (pawn.Faction.def == FactionDefsYWD.GlitterFactionYWD && !pawn.IsPrisoner)
                {
                    if (!pawn.health.hediffSet.HasHediff(HeDiffDefsYWD.GlitterCombatBoosterYWD))
                    {
                        pawn.health.AddHediff(HeDiffDefsYWD.GlitterCombatBoosterYWD);
                        pawn.health.AddHediff(HeDiffDefsYWD.GlitterHealingMechanitesYWD);
                    }  
                }
            }
        }
    }
}
