﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RimWorld;
using Verse;

namespace YouWillDie
{
    public class GlitterHealingMechanitesYWD : HediffWithComps
    {
        public int ticksTillNextHeal = 0;

        public override void Tick()
        {
            base.Tick();

            Pawn parentPawn = this.pawn;

            //Kill pawn if they are downed (both legs destroyed so similar)
            if (parentPawn.Downed)
            {
                parentPawn.Kill(null, null);
            }

            if (ticksTillNextHeal < Find.TickManager.TicksGame)
            {
                List<Hediff> hediffsList = parentPawn.health.hediffSet.hediffs;
                foreach (Hediff hediff in hediffsList)
                {
                    Hediff_Injury hediff_Injury = hediff as Hediff_Injury;
                    if (hediff_Injury != null && hediff_Injury.CanHealNaturally())
                    {
                        hediff_Injury.Heal(0.1f);
                        Severity -= 0.002f;
                        base.pawn.health.Notify_HediffChanged(this);
                    }
                }
                ticksTillNextHeal = Find.TickManager.TicksGame + 10;
            }
        }
    }
}
