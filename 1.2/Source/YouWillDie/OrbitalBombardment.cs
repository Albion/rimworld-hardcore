﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RimWorld;
using RimWorld.BaseGen;
using UnityEngine;
using Verse;
using Verse.AI;

namespace YouWillDie
{
    
    public class MapComponent_OrbitalBombardmentYWD : MapComponent
    {
        public bool active = false;

        public bool targetTurrets = false;

        public bool openRansomDemand = false;

        public int ransomDeadlineTick = -1;

        public int tickAtNextVolley = -1;

        public int endAtTick = -1;

        public IntRange volleyTimeout = new IntRange(180, 250);

        public IntVec3 volleyTarget;

        public MapComponent_OrbitalBombardmentYWD(Map map) : base(map)
        {
        }

        public override void MapGenerated()
        {
            base.MapGenerated();
        }

        public override void MapRemoved()
        {
            base.MapRemoved();
            StopBombing();
        }

        public void StartBombing()
        {
            this.active = true;
            this.volleyTarget = PickNewSpotToBomb();
            this.endAtTick = -1;
        }

        public IntVec3 StartBombing(int bombardmentDuration)
        {
            StartBombing();
            this.endAtTick = Find.TickManager.TicksGame + bombardmentDuration;
            return volleyTarget;
        }

        public void StopBombing()
        {
            this.active = false;
        }

        public override void MapComponentTick()
        {
            base.MapComponentTick();

            //Check if there is an open ransom demand
            if (openRansomDemand)
            {
                if(Find.TickManager.TicksGame >= this.ransomDeadlineTick)
                {
                    Find.LetterStack.ReceiveLetter("LetterLabelRansomNotMetYWD".Translate(), "LetterRansomNotMetYWD".Translate(), LetterDefOf.NegativeEvent, new TargetInfo(StartBombing(2000), map), null);
                    openRansomDemand = false;
                }
            }

            //Check if bombardment is active, otherwise return
            if (!this.active)
            {
                return;
            }
            //if there is a end-Tick set deactiveate event on reaching this tick
            if(Find.TickManager.TicksGame >= this.endAtTick && this.endAtTick > 0)
            {
                StopBombing();
                return;
            }
            if(Find.TickManager.TicksGame >= tickAtNextVolley)
            {
                //mostly stolen from Verb_PowerBeam
                BombardmentBeamYWD powerBeam = (BombardmentBeamYWD)GenSpawn.Spawn(ThingDefsYWD.OribitalBombardmentBeamYWD, this.volleyTarget, this.map, WipeMode.Vanish);
                powerBeam.duration = 400;
                powerBeam.StartStrike();

                CalculateNextBombardmentTick();
                this.volleyTarget = PickNewSpotToBomb();
                DrawTargeterBeam(this.volleyTarget);
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look<int>(ref this.tickAtNextVolley, "tickAtNextVolley", -1, false);
            Scribe_Values.Look<int>(ref this.endAtTick, "endAtTick", -1, false);
            Scribe_Values.Look<int>(ref this.ransomDeadlineTick, "ransomDeadlineTick", -1, false);
            Scribe_Values.Look<bool>(ref this.openRansomDemand, "openRansomDemand", false, false);
            Scribe_Values.Look<bool>(ref this.active, "active", false, false);
            Scribe_Values.Look<bool>(ref this.targetTurrets, "targetTurrets", false, false);
            Scribe_Values.Look<IntVec3>(ref this.volleyTarget, "volleyTarget");
        }

        public IntVec3 PickNewSpotToBomb()
        {
            IntVec3 intvec;
            //Target Turrets if this was selected
            if (targetTurrets)
            {
                if(TryTurretPosition(out intvec))
                {
                    return intvec;
                }
            }
            //Find a spot in or near the colony
            if(DropCellFinder.TryFindRaidDropCenterClose(out intvec, map, true, true))
            {
                return intvec;
            }
            //if no place was found that satisfies the above conditions just bomb anything.
            return CellFinder.RandomNotEdgeCell(10, this.map);
        }

        protected bool TryTurretPosition(out IntVec3 intVec)
        {
            if (this.map.listerBuildings.AllBuildingsColonistOfClass<Building_Turret>().TryRandomElement(out Building_Turret turret))
            {
                intVec = turret.Position;
                return true;
            }
            intVec = new IntVec3();
            return false;
        }

        public void CalculateNextBombardmentTick()
        {
            this.tickAtNextVolley = Find.TickManager.TicksGame + volleyTimeout.RandomInRange;
        }

        public void DrawTargeterBeam(IntVec3 position)
        {
            if (!position.IsValid || !position.InBounds(this.map))
            {
                position = PickNewSpotToBomb();
            }
            MoteMaker.MakeBombardmentMote(position, this.map);
        }

    }


    public class BombardmentBeamYWD : OrbitalStrike
    {
        //Custom class simply copied and adjusted from vanilla PowerBeam

        //reduced from 15
        public const float Radius = 4f;

        //reduced from 4
        private const int FiresStartedPerTick = 2;

        private static readonly IntRange FlameDamageAmountRange = new IntRange(30, 60);

        private static readonly IntRange CorpseFlameDamageAmountRange = new IntRange(5, 10);

        private static List<Thing> tmpThings = new List<Thing>();

        public override void StartStrike()
        {
            base.StartStrike();
            this.MakePowerBeamMote(base.Position, base.Map);
        }

        //copied from vanilla motemaker
        public void MakePowerBeamMote(IntVec3 cell, Map map)
        {
            Mote mote = (Mote)ThingMaker.MakeThing(ThingDefOf.Mote_PowerBeam, null);
            mote.exactPosition = cell.ToVector3Shifted();
            mote.Scale = 30f;
            mote.rotationRate = 1.28f;
            GenSpawn.Spawn(mote, cell, map, WipeMode.Vanish);
        }

        public override void Tick()
        {
            base.Tick();
            if (!base.Destroyed)
            {
                for (int i = 0; i < FiresStartedPerTick; i++)
                {
                    this.StartRandomFireAndDoFlameDamage();
                }
            }
        }

        private void StartRandomFireAndDoFlameDamage()
        {
            IntVec3 c = (from x in GenRadial.RadialCellsAround(base.Position, Radius, true)
                         where x.InBounds(base.Map)
                         select x).RandomElementByWeight((IntVec3 x) => 1f - Mathf.Min(x.DistanceTo(base.Position) / 15f, 1f) + 0.05f);
            FireUtility.TryStartFireIn(c, base.Map, Rand.Range(0.1f, 0.925f));
            BombardmentBeamYWD.tmpThings.Clear();
            BombardmentBeamYWD.tmpThings.AddRange(c.GetThingList(base.Map));
            for (int i = 0; i < BombardmentBeamYWD.tmpThings.Count; i++)
            {
                int num = (!(BombardmentBeamYWD.tmpThings[i] is Corpse)) ? BombardmentBeamYWD.FlameDamageAmountRange.RandomInRange : BombardmentBeamYWD.CorpseFlameDamageAmountRange.RandomInRange;
                Pawn pawn = BombardmentBeamYWD.tmpThings[i] as Pawn;
                //No log entries because there is no instigator
                Thing thing = BombardmentBeamYWD.tmpThings[i];
                DamageDef flame = DamageDefOf.Flame;
                float amount = (float)num;
                Thing instigator = this.instigator;
                ThingDef weaponDef = this.weaponDef;
                thing.TakeDamage(new DamageInfo(flame, amount, 0f, -1f, instigator, null, weaponDef, DamageInfo.SourceCategory.ThingOrUnknown, null));
            }
            BombardmentBeamYWD.tmpThings.Clear();
        }

    }

}
