﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RimWorld;
using Verse;

namespace YouWillDie
{
    [DefOf]
    public static class ThingDefsYWD
    {
        public static ThingDef OribitalBombardmentBeamYWD;
    }

    [DefOf]
    public static class IncidentDefsYWD
    {
        public static IncidentDef GlitterRaidYWD;
    }

    [DefOf]
    public static class FactionDefsYWD
    {
        public static FactionDef GlitterFactionYWD;
    }

    [DefOf]
    public static class HeDiffDefsYWD
    {
        public static HediffDef GlitterCombatBoosterYWD;

        public static HediffDef GlitterHealingMechanitesYWD;
    }

    [DefOf]
    public static class LetterDefsYWD
    {
        public static LetterDef RansomOrBombardmentDemand;
    }
}
