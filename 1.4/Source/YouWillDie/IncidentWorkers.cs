﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RimWorld;
using Verse;
using UnityEngine;

namespace YouWillDie
{
    public class IncidentWorker_AnyBombingEventYWD : IncidentWorker
    {
        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            //Either trigger Bomb and raid or fire indescriminate bombardment
            if (Rand.Chance(0.7f))
            {
                return TryExecuteBombAndRaid(parms);
            }
            return TryExecuteRandomBombardment(parms);
        }

        protected bool TryExecuteBombAndRaid(IncidentParms parms)
        {
            int ticksBeforeRaid = 2500;
            IntVec3 firstTarget;

            Map map = (Map)parms.target;
            MapComponent_OrbitalBombardmentYWD bombardmentComp = map.GetComponent<MapComponent_OrbitalBombardmentYWD>();

            //Decide if target security or random bombardment
            if (Rand.Chance(0.8f))
            {
                bombardmentComp.targetTurrets = true;
                firstTarget = bombardmentComp.StartBombing(ticksBeforeRaid);
            }
            else
            {
                bombardmentComp.targetTurrets = false;
                firstTarget = bombardmentComp.StartBombing(ticksBeforeRaid);
            }

            Find.LetterStack.ReceiveLetter("LetterLabelPreemptiveBombardmentYWD".Translate(), "LetterPreemptiveBombardmentYWD".Translate(), LetterDefOf.NegativeEvent, new TargetInfo(firstTarget, map), null);

            FactionAdditionYWD.EnsureFactionExists(FactionDefsYWD.GlitterFactionYWD);

            IncidentParms incidentParms = parms;
            incidentParms.faction = Find.FactionManager.FirstFactionOfDef(FactionDefsYWD.GlitterFactionYWD) ?? Find.FactionManager.OfMechanoids;
            incidentParms.raidStrategy = RaidStrategyDefOf.ImmediateAttack;
            incidentParms.raidArrivalMode = PawnsArrivalModeDefOf.CenterDrop;
            QueuedIncident queuedIncident = new QueuedIncident(new FiringIncident(IncidentDefOf.RaidEnemy, null, incidentParms), Find.TickManager.TicksGame + ticksBeforeRaid);
            Find.Storyteller.incidentQueue.Add(queuedIncident);
            return true;
        }

        protected bool TryExecuteRandomBombardment(IncidentParms parms)
        {
            int bombardmentDuration = 3000;
            IntVec3 firstTarget;

            Map map = (Map)parms.target;
            MapComponent_OrbitalBombardmentYWD bombardmentComp = map.GetComponent<MapComponent_OrbitalBombardmentYWD>();

            bombardmentComp.targetTurrets = false;
            firstTarget = bombardmentComp.StartBombing(bombardmentDuration);

            Find.LetterStack.ReceiveLetter("LetterLabelBombardmentYWD".Translate(), "LetterBombardmentYWD".Translate(), LetterDefOf.NegativeEvent, new TargetInfo(firstTarget, map), null);

            return true;
        }
    }

    /*public class IncidentWorker_BombAndRaidYWD : IncidentWorker
    {
        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            int ticksBeforeRaid = 2500;
            IntVec3 firstTarget;

            Map map = (Map)parms.target;
            MapComponent_OrbitalBombardmentYWD bombardmentComp = map.GetComponent<MapComponent_OrbitalBombardmentYWD>();

            //Decide if target security or random bombardment
            if (Rand.Chance(0.8f))
            {
                bombardmentComp.targetTurrets = true;
                firstTarget = bombardmentComp.StartBombing(ticksBeforeRaid);
            }
            else
            {
                bombardmentComp.targetTurrets = false;
                firstTarget = bombardmentComp.StartBombing(ticksBeforeRaid);
            }

            Find.LetterStack.ReceiveLetter("LetterLabelPreemptiveBombardmentYWD".Translate(), "LetterPreemptiveBombardmentYWD".Translate(), LetterDefOf.NegativeEvent, new TargetInfo(firstTarget, map), null);

            FactionAdditionYWD.EnsureFactionExists(FactionDefsYWD.GlitterFactionYWD);

            IncidentParms incidentParms = parms;
            incidentParms.faction = Find.FactionManager.FirstFactionOfDef(FactionDefsYWD.GlitterFactionYWD) ?? Find.FactionManager.OfMechanoids;
            incidentParms.raidStrategy = RaidStrategyDefOf.ImmediateAttack;
            incidentParms.raidArrivalMode = PawnsArrivalModeDefOf.CenterDrop;
            QueuedIncident queuedIncident = new QueuedIncident(new FiringIncident(IncidentDefOf.RaidEnemy, null, incidentParms), Find.TickManager.TicksGame + ticksBeforeRaid);
            Find.Storyteller.incidentQueue.Add(queuedIncident);
            return true;
        }
    }*/

    public class IncidentWorker_GlitterRaidYWD : IncidentWorker_RaidEnemy
    {
        protected bool fireSolarFlare(IncidentParms parms)
        {
            GameConditionManager gameConditionManager = parms.target.GameConditionManager;
            if (gameConditionManager == null)
            {
                return false;
            }
            if (gameConditionManager.ConditionIsActive(GameConditionDefOf.SolarFlare))
            {
                return false;
            }
            List<GameCondition> activeConditions = gameConditionManager.ActiveConditions;
            for (int i = 0; i < activeConditions.Count; i++)
            {
                if (!GameConditionDefOf.SolarFlare.CanCoexistWith(activeConditions[i].def))
                {
                    return false;
                }
            }

            int duration = Rand.RangeInclusive(40000, 90000);
            GameCondition gameCondition = GameConditionMaker.MakeCondition(GameConditionDefOf.SolarFlare, duration);
            gameConditionManager.RegisterCondition(gameCondition);
            return true;
        }
        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            fireSolarFlare(parms);

            FactionAdditionYWD.EnsureFactionExists(FactionDefsYWD.GlitterFactionYWD);

            parms.faction = Find.FactionManager.FirstFactionOfDef(FactionDefsYWD.GlitterFactionYWD) ?? Find.FactionManager.OfMechanoids;
            parms.raidStrategy = RaidStrategyDefOf.ImmediateAttack;
            parms.raidArrivalMode = PawnsArrivalModeDefOf.CenterDrop;
            if (!base.TryExecuteWorker(parms))
                {
                    return false;
                }
                Find.TickManager.slower.SignalForceNormalSpeedShort();
                return true;
        }

        protected override string GetLetterText(IncidentParms parms, List<Pawn> pawns)
        {
            string text = base.GetLetterText(parms, pawns);
            text += "\n\n";
            text += "LetterSolarFlareModificationYWD".Translate();
            return text;
        }
    }

    /*public class IncidentWorker_RandomBombingYWD : IncidentWorker
    {
        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            int bombardmentDuration = 3000;
            IntVec3 firstTarget;

            Map map = (Map)parms.target;
            MapComponent_OrbitalBombardmentYWD bombardmentComp = map.GetComponent<MapComponent_OrbitalBombardmentYWD>();

                bombardmentComp.targetTurrets = false;
                firstTarget = bombardmentComp.StartBombing(bombardmentDuration);

            Find.LetterStack.ReceiveLetter("LetterLabelBombardmentYWD".Translate(), "LetterBombardmentYWD".Translate(), LetterDefOf.NegativeEvent, new TargetInfo(firstTarget, map), null);

            return true;
        }
    }*/

    public class IncidentWorker_RansomOrBombingYWD : IncidentWorker
    {
        protected override bool CanFireNowSub(IncidentParms parms)
        {
            return base.CanFireNowSub(parms) && CommsConsoleUtility.PlayerHasPoweredCommsConsole();
        }

        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            int fee = Mathf.RoundToInt(StorytellerUtility.DefaultThreatPointsNow(parms.target));
            Map map = TradeUtility.PlayerHomeMapWithMostLaunchableSilver();
            ChoiceLetter_RansomOrBombDemandYWD choiceLetter_BombingRansomDemand = (ChoiceLetter_RansomOrBombDemandYWD)LetterMaker.MakeLetter("LetterLabelRansomYWD".Translate(), "LetterRansomYWD".Translate(fee), LetterDefsYWD.RansomOrBombardmentDemand);

            MapComponent_OrbitalBombardmentYWD bombardmentComp = map.GetComponent<MapComponent_OrbitalBombardmentYWD>();
            bombardmentComp.openRansomDemand = true;
            bombardmentComp.ransomDeadlineTick = Find.TickManager.TicksGame + 60000;

            choiceLetter_BombingRansomDemand.title = "LetterLabelRansomYWD".Translate();
            choiceLetter_BombingRansomDemand.radioMode = true;
            choiceLetter_BombingRansomDemand.map = map;
            choiceLetter_BombingRansomDemand.fee = fee;
            choiceLetter_BombingRansomDemand.StartTimeout(60000);

            Find.LetterStack.ReceiveLetter(choiceLetter_BombingRansomDemand, null);
            return true;
        }
    }
}
